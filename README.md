# OpenLP Full Verse Stage View
A custom stage view for OpenLP that displays the full verses according to a theme, regardless of whether it has been broken into smaller chunks for the main display.

# Introduction  
At our church, we livestream services onto YouTube, and during worship, project lyrics into a lower thirds bar at the bottom of the screen. OpenLP is great for this, as it automatically breaks verses into chunks to fit the height of the theme you have set, saving a lot of work, and meaning an existing song database can be used for lower thirds without modification.

It is preferable, however, for the live congregation to see full verses, rather than two lines at a time, so that the time of worship doesn't turn into karaoke, and so the congregation can reflect on the meaning of the words they are singing in the context of the whole verse. Unfortunately, this would currently require a second person to separately control another computer running OpenLP, projecting the full verses.

In an ideal world, OpenLP would support a second output using a separate theme. Perhaps one day I'll learn how OpenLP works and write an addon for it - in the mean time, this custom stage view gives basic functionality.

# Usage
To use this custom stage view, perform the following steps:

1. In OpenLP, click on **Tools** in the toolbar, and then **Open Data Folder**. Inside the data folder create a folder named **stages**, inside that a folder named **full-verse**, and place the files inside that. Your folder structure should look like this:

```
+-- < OpenLP Data Folder >
|   +-- stages
|   |   +-- full-verse
|   |   |   +-- stage.html
|   |   |   +-- script.js
|   |   |   +-- obs-websocket.min.js
```
2. Next you will need to create a theme called **Full_Verse** (case sensitive), which will set any colours, alignment and positioning of the full verse view. Note that while the position can be set, the content height will not have an effect, and a full verse will be shown regardless.

3. Finally, enable the built-in web remote plugin in OpenLP. Then, on the same computer or another on the same network, navigate to <remote_url>:4316/stage/full-verse. The display will be the same size as your main output, so you may need to enable full screen to see the footer. You can find your IP in OpenLP by going to settings, remote interface, and the links will show you. If you are using this on the same PC as is projecting (such as on a third screen) you can [replace the IP address with 'localhost'](http://localhost:4316/stage/full-verse).

You should now be able to change verse in OpenLP, and see the verse change on the stage view.

<div align="center">
    <img src="https://i.ibb.co/bRzrKm6/openlp-full-verse-screenshot-1.png" alt="openlp-full-verse-screenshot-1">
</div>

Once initially connected to OpenLP, this custom stage will automatically try to reconnect if the connection is lost - when OpenLP is running again, it will start updating on the next transition. Hopefully in a live context this will recover any software or network problems that may occur with minimal disruption! 

Further instructions can be found here:

[https://manual.openlp.org/stage_view.html](https://manual.openlp.org/stage_view.html)

# Limitations
Unfortunately, this only works for song lyrics, Bible verses, static presentations and images at the moment (note that for Bible verses you may need to set your lower thirds template in the Bible settings). Images and presentations are being rendered using screenshots of the output screen, so dynamic presentation content such as moving elements or embedded videos will not work.

The ideal scenario would be a plugin that adds an extra screen, linked to the verse changes of the main display, but mirroring any other media - perhaps one day in the future that will be possible. For a solution for video, see the OBS section below.

This has been tested and works on the latest version 2.9.2 - I am not sure if it works on older versions, but you can download 2.9.2 [here](https://get.openlp.org/).

# OBS - Media
The primary limitation of this setup is that media such as videos do not work. In some scenarios, this is totally fine - if the displays in your building are driven by a video mixer, you can simply have the Full Verse browser as one source, the main output (lower thirds) feed as another, and change over for any media items. If your in-house screens can't do this, though, it is possible to achieve this in a slightly more complex way using OBS.

OBS is a really powerful open-source software package for video mixing, which we can use to change the output automatically. Clever people have written an extension for it to provide websocket control, and other clever people have written [JS bindings](https://github.com/haganbmj/obs-websocket-js) for it, which are included with this package. This custom stage will try and connect to the websocket server on the default port of 4444 - if it gets a connection, it will automatically switch OBS to a scene called "Lower Thirds" when media is played, and will change back to one called "Full Verse" for anything else. If it doesn't connect to OBS, it will work as normal in the browser. 

To set this up, do the following:

1. Install [OBS](https://obsproject.com/)

2. Install the [websockets plugin](https://github.com/Palakis/obs-websocket/releases), leave it on the default port of 4444 and turn off authentication.

3. Set up OBS
   - change the canvas and output resolution to your desired resolution
   - create 2 scenes called "Full Verse" and "Lower Thirds"
   - add a browser source to Full Verse pointing at http://localhost:4316/stage/full-verse
   - add a screen capture source to Lower Thirds pointing at the main output display screen

4. Finally, right click on the preview section, select "Fullscreen Projector (Preview)", and then the screen you want to show in the room.

You should now be able to minimise the OBS control window, and the output should change automatically between scenes depending on which type of content OpenLP is showing!

You may need to play with firewall settings if it does not connect automatically, but I have succesfully tested this on Linux Mint and Windows 10.

# Updates

> 04/05/21 - added OBS media support!

> 26/04/21 - auto-reconnect when connection to OpenLP lost

> 26/04/21 - fixed bug for songs with repeated verses concatenating

> 25/04/21 - fixed bug that caused OpenLP to crash when changing from media back to a song

> 03/03/21 - added static presentation support

> 03/03/21 - added image support

> 02/03/21 - show theme background for non-text content 

> 02/03/21 - added Bible support, by realising it worked already...

> 02/03/21 - added blank to theme or black support

- Ant Weedon, 2021