// Websockets
const host = window.location.hostname;
const websocketPort = 4317;
var websocket = null;					// the websocket connection
var pingInterval = null;		// store the interval for lost connection so it can be cancelled

// OpenLP Data
var currentItem = null;
var currentVerse = '';
var curSongLong = {};		// current song, with complete verses
var curSongShort = {};	// current song, verses broken smaller
var curSongFooter = '';
var theme = null;
var item = null;
var media = false;				// used to stop errors when changing from media

// constants
const textAlignment = ['left', 'right', 'center', 'justify'];

// OBS websockets
var obs = false;
const obs_socket = new OBSWebSocket();

obs_socket.connect({address: 'localhost:4444'})
.then(() => {
	obs_socket.send('SetCurrentScene', {
		'scene-name': 'Full Verse'
	});
	obs = true;
})
.catch((err) => {
	console.log(err);
	console.log('OBS not found.')
	obs = false;
});

start();

// start a websocket connection
function start() {
	websocket = new WebSocket(`ws://${host}:${websocketPort}`);
	websocket.addEventListener('message', onMessage);
	websocket.addEventListener('close', onClose);
}

// update when a message received on websocket
function onMessage(event) {
	const reader = new FileReader();
	reader.onload = async () => {
		const state = JSON.parse(reader.result.toString()).results;

		// set blank colour to theme if theme set
		if (state.theme) {
			if (theme == null) await setTheme();
			document.getElementById('blank').style.backgroundColor = theme.background_color;
		}

		// set blank colour to black if black set
		if (state.blank) document.getElementById('blank').style.backgroundColor = 'black';

		// blank screen if theme or blank set
		document.getElementById('blank').style.opacity = (state.blank || state.theme) ? '1' : '0';

		if (state.item == "") return;
		// Check if current item has changed, and load new song if it has
		// if current (ie, previous) item was media, skip this time
		if (currentItem != state.item && !media) {
			currentItem = state.item;

			// get current item
			item = await getItem();

			// set media to true when we have a media item
			if (item.name == 'media') {
				media = true;
				if (obs) {
					obs_socket.send('SetCurrentScene', {
						'scene-name': 'Lower Thirds'
					});
				}
			} else {
				if (obs) {
					obs_socket.send('SetCurrentScene', {
						'scene-name': 'Full Verse'
					});
				}
			}

			if (item.type != 'ServiceItemType.Text') {
				// if the item isn't text, clear all text
				document.getElementById('content').innerHTML = '';
				document.getElementById('footer').innerText = '';
				curSongLong = {};
				curSongShort = {};
				curSongFooter = '';
				currentVerse = '';
			} else {
				// if it is text, update all text
				let data = await getCurrentSong();
				[curSongLong, curSongShort, curSongFooter] = data;
				currentVerse = '';
			}
		}

		// reset media variable after first item change
		if (currentItem != state.item && media) media = false;

		// if there is no item, wait for one (at first startup)
		if (item == null) return;

		// show image or presentation
		if (item.type == 'ServiceItemType.Image' || item.name == "presentations") {
			let image = await getImage();
			// set image background image and show it
			document.getElementById('image').src = image.binary_image;
			document.getElementById('image').style.opacity = '1';
		} else {
			// hide image
			document.getElementById('image').style.opacity = '0';
		}

		// Check if verse has changed, update html if it has
		if (item.type == 'ServiceItemType.Text') {
			let verse = getCurrentVerse(state.slide);
			if (verse != currentVerse) {
				// change to new slide
				currentVerse = verse;
				showVerse(verse);
			}
		}
	};
	reader.readAsText(event.data);
};

// start pinging to reconnect when websocket connection lost
function onClose() {
	pingInterval = setInterval(ping, 2000);
}

function applyTheme() {
	let content = document.getElementById('content');
	let footer = document.getElementById('footer');

	document.body.style.backgroundColor = theme.background_color;

	// Main Content
	if (theme.font_main_bold) content.style.fontWeight = 'bold';
	if (theme.font_main_italics) content.style.fontStyle = 'italic';
	content.style.height = theme.font_main_height;
	content.style.color = theme.font_main_color;
	content.style.fontFamily = theme.font_main_name;
	content.style.fontSize = theme.font_main_size+'pt';
	content.style.marginLeft = theme.font_main_x+'px';
	content.style.marginTop = theme.font_main_y+'px';
	content.style.textAlign = textAlignment[theme.display_horizontal_align];
	if (theme.font_main_outline) {
		content.style['-webkit-text-stroke'] = theme.font_main_outline_size + 'px ' + theme.font_main_outline_color;
	}
	if (theme.font_main_shadow) {
		content.style['text-shadow'] = '0 0 '+theme.font_main_shadow_size + 'px ' + theme.font_main_shadow_color;
	}

	// Footer
	if (theme.font_footer_bold) footer.style.fontWeight ='bold';
	if (theme.font_footer_italics) footer.style.fontStyle ='italic';
	footer.style.color = theme.font_footer_color;
	footer.style.fontFamily = theme.font_footer_name;
	footer.style.fontSize = theme.font_footer_size+'pt';
	footer.style.position = 'fixed';
	footer.style.left = theme.font_footer_x+'px';
	footer.style.top = theme.font_footer_y+'px';
	if (theme.font_footer_outline) {
		footer.style['-webkit-text-stroke'] = theme.font_footer_outline_size + 'px ' + theme.font_footer_outline_color;
	}
	if (theme.font_footer_shadow) {
		footer.style['text-shadow'] = '0 0 '+theme.font_footer_shadow_size + 'px ' + theme.font_footer_shadow_color;
	}
}

async function getCurrentSong() {
	let items = await getItems();

	// regenerate complete verses from fragments
	let slides = {};
	slides['song_title'] = items.audit[0];

	let currentItem = items.slides[0].tag;
	let added = [];
	for (slide of items.slides) {
		// only add the first instance of a verse
		if (slide.tag != currentItem) {
			added.push(currentItem);
			if (added.includes(slide.tag)) continue;
			currentItem = slide.tag;
		}
		// create a new verse, or append to current
		if (slide.tag in slides) {
			slides[slide.tag] = slides[slide.tag] + '<br>' + slide.html;
		} else {
			// add new verse to slides
			slides[slide.tag] = slide.html;
		}
	}

	// construct footer
	let footer = '';
	for (entry of items.audit)
		if (entry != 'None' && entry != null) {
			if (footer == '') {
				footer = entry;
			} else {
				footer += ', ' + entry;
			}
		}

	return [slides, items.slides, footer];
}

// convert slide number in smaller lines array into verse tag
function getCurrentVerse(slide) {
	return curSongShort[slide].tag;
}

function getItem() {
	return fetch('http://' + window.location.host + '/api/v2/controller/live-item', {
		method: "GET", 
	}).then(res => {
		return res.json();
	}).catch((error) => {
		throw 'Exception';
	});
}

function getImage() {
	return fetch('http://' + window.location.host + '/api/v2/core/live-image', {
		method: "GET", 
	}).then(res => {
		return res.json();
	});
}

function getItems() {
	return fetch('http://' + window.location.host + '/api/v2/controller/live-items', {
		method: "GET", 
	}).then(res => {
		return res.json();
	});
}

function getTheme() {
	return fetch('http://' + window.location.host + '/api/v2/controller/themes/Full_Verse', {
        method: "GET", 
    }).then(res => {
        return res.json();
    });
}

// detect when server back up again and reconnect
async function ping() {
	try {
		let test = await getItem();
		if (test == null) throw 'Test is null';
		console.log('Reconnected');
		clearInterval(pingInterval);
		start();
	} catch (e) {
		console.log('Connection lost: trying to reconnect...');
	}
}

window.setTheme = async function() {
	if (theme == null) theme = await getTheme();
	applyTheme();
};

function showVerse() {
	document.getElementById('content').innerHTML = curSongLong[currentVerse];
	document.getElementById('footer').innerText = curSongFooter;
}